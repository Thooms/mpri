#! /bin/bash
#
# Kill (or at least send SIGKILL) to all the processus named $1
#


for id in $( grep $1 /proc/*/status | awk -F '/' '{print $1}' ); do
    kill -9 $id
done
