#! /bin/bash

# Fetch the zip, and extract it.

curl http://www.lsv.ens-cachan.fr/~schwoon/enseignement/systemes/ws1213/tp1fichiers.zip >> tmp.zip \
    && unzip tmp -d tp1fichiers \
    && rm tmp.zip

# To list in order of modification date

ls -t

# See ./question3.sh for the script of the next question.

 ./question3.sh foo bar

# Thumbnails creation

cd tp1fichiers

for im in *.jpg; do
    convert $im -thumbnail 200x200 "small-"$im
done

cd ..

# Football team thing: cf question5.sh

./question5.sh Monaco